module quote

go 1.13

require (
	github.com/stretchr/testify v1.10.0
	golang.org/x/text v0.22.0
	rsc.io/sampler v1.3.0
)
