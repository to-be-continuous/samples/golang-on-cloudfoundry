package main

import (
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestQuoteHandler(t *testing.T) {
	// Table Driven Test (more info at https://github.com/golang/go/wiki/TableDrivenTests)
	testCase := []struct {
		headers      map[string]string
		expectedBody string
	}{
		{
			expectedBody: "Hello, world.",
		},
		{
			headers:      map[string]string{"Accept-Language": "de"},
			expectedBody: "Hello, world.",
		},
		{
			headers:      map[string]string{"Accept-Language": "fr"},
			expectedBody: "Bonjour le monde.",
		},
		{
			headers:      map[string]string{"Accept-Language": "fr-FR"},
			expectedBody: "Bonjour le monde.",
		},
		{
			headers:      map[string]string{"Accept-Language": "si"},
			expectedBody: "හෙලෝ වර්ල්ඩ්.",
		},
		{
			headers:      map[string]string{"Accept-Language": "zh"},
			expectedBody: "你好，世界。",
		},
	}

	for _, tt := range testCase {
		tt := tt // it seems odd but plz read https://golang.org/doc/faq#closures_and_goroutines or https://github.com/golang/go/wiki/CommonMistakes#using-goroutines-on-loop-iterator-variables
		t.Run(
			"Body response should contain "+tt.expectedBody,
			func(t *testing.T) {
				t.Parallel()
				var (
					r = httptest.NewRequest(http.MethodGet, "http://example.com/", nil)
					w = httptest.NewRecorder()
				)

				for k, v := range tt.headers {
					r.Header.Add(k, v)
				}

				quoteHandler(w, r)

				var (
					resp    = w.Result()
					body, _ = io.ReadAll(resp.Body)
				)

				assert.Equal(t, http.StatusOK, resp.StatusCode)
				assert.Equal(t, tt.expectedBody, string(body))
			},
		)
	}
}
