package main

import (
	"log"
	"net/http"
	"os"
	"time"
)

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func main() {
	port := getEnv("PORT", "8080")
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		quoteHandler(w, r)
	})
	server := &http.Server{
		Addr:              ":" + port,
		ReadHeaderTimeout: 3 * time.Second,
	}
	log.Print("Starting server on port " + port + "...")
	log.Fatal(server.ListenAndServe())
}
