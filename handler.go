package main

import (
	"log"
	"net/http"

	"golang.org/x/text/language"
	"rsc.io/sampler"
)

var matcher = language.NewMatcher(
	[]language.Tag{
		language.English, // The first language is used as fallback.
		language.French,
		language.Sinhala,
		language.Chinese,
	},
)

func quoteHandler(w http.ResponseWriter, r *http.Request) {
	var (
		lang, _ = r.Cookie("lang")
		accept  = r.Header.Get("Accept-Language")
		tag, _  = language.MatchStrings(matcher, lang.String(), accept)
	)

	s := ""
	for s == "" {
		s = sampler.Hello(tag)
		tag = tag.Parent()
	}

	_, err := w.Write([]byte(s))
	if err != nil {
		log.Println("error has occurred while writing response", err)
	}
}
