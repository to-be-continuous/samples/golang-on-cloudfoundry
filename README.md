# Go on Cloud Foundry project sample

This project sample shows the usage of _to be continuous_ templates:

* Go
* SonarQube (from [sonarcloud.io](https://sonarcloud.io/))
* Cloud Foundry

The project exposes a basic web server, testable with web browser.

:warning: We don't have any Cloud Foundry organization/space in the public cloud to host our samples _yet_.
So as a matter of fact this project won't deploy, but it works I swear :)

## Go template features

This project uses the default Go template configuration.

The Go template natively implements [unit tests report](https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html) and
[code coverage](https://docs.gitlab.com/ee/ci/testing/code_coverage.html) integration in GitLab.

## SonarQube template features

This project uses the following features from the SonarQube template:

* Defines the `SONAR_HOST_URL` (SonarQube server host),
* Defines `organization` & `projectKey` from [sonarcloud.io](https://sonarcloud.io/) in `sonar-project.properties`,
* Defines :lock: `$SONAR_TOKEN` as secret CI/CD variable,
* Uses the `sonar-project.properties` to specify project specific configuration:
    * source and test folders,
    * code coverage report,
    * unit test reports.

## Cloud Foundry template features

This project uses the following features from the Cloud Foundry template:

* Defines mandatory `$CF_URL` and `$CF_ORG` in the `.gitlab-ci.yml` file,
* Defines mandatory :lock: `$CF_USER` and :lock: `$CF_PASSWORD` credentials as secret CI/CD variables,
* Enables review environments by declaring the `$CF_REVIEW_SPACE` in the `.gitlab-ci.yml` file,
* Enables staging environment by declaring the `$CF_STAGING_SPACE` in the `.gitlab-ci.yml` file,
* Enables production environment by declaring the `$CF_PROD_SPACE` in the `.gitlab-ci.yml` file.
